//
// Created by theoh on 26/10/2020.
//
#include <iostream>
#include "math.h"

float x0[2];

double phiPrime(float deriveePartielleF_x, float deriveePartielleF_y) {
    double a = 4 * deriveePartielleF_x * deriveePartielleF_x * deriveePartielleF_x * deriveePartielleF_x + 4 * deriveePartielleF_y * deriveePartielleF_y * deriveePartielleF_y * deriveePartielleF_y;
    double b = 12 * x0[0] * deriveePartielleF_x * deriveePartielleF_x * deriveePartielleF_x + 12 * x0[1] * deriveePartielleF_y * deriveePartielleF_y * deriveePartielleF_y;
    double c = 2 * deriveePartielleF_x * deriveePartielleF_x - 4 * deriveePartielleF_x * deriveePartielleF_y + 2 * deriveePartielleF_y * deriveePartielleF_y + 12 * x0[0] * x0[0] * deriveePartielleF_x * deriveePartielleF_x + 12 * x0[1] * x0[1] * deriveePartielleF_y * deriveePartielleF_y;
    double d = 2 * x0[0] * deriveePartielleF_x - 2 * x0[0] * deriveePartielleF_y - 2 * x0[1] * deriveePartielleF_x + 2 * x0[1] * deriveePartielleF_y + 4 * x0[0] * x0[0] * x0[0] * deriveePartielleF_x + 4 * x0[1] * x0[1] * x0[1] * deriveePartielleF_y;
    double p = (3 * a * c - b * b) / (3 * a * a);
    double q = (2 * b * b * b - 9 * a * b * c + 27 * a * a * d) / (27 * a * a * a);
    double delta1 = q * q + ((4 * p * p * p) / 27);
    double racine_delta1 = sqrt(delta1);
    double resultat1 = (cbrt((-q - racine_delta1) / 2) + cbrt((-q + racine_delta1) / 2) - (b / (3 * a)));
    double delta2 = (b + a * resultat1) * (b + a * resultat1) - 4 * a * (c + (b + a * resultat1) * resultat1);
    double racine_delta2;
    if (delta2 < 0) {
        racine_delta2 = sqrt(-delta2);
    }
    else {
        racine_delta2 = sqrt(delta2);
    }
    double resultat2 = (-b - a * resultat1 - racine_delta2) / (2 * a);
    double resultat3 = (-b - a * resultat1 + racine_delta2) / (2 * a);
    resultat1 = abs(round(resultat1 * 10000.0f) / 10000.0f);
    resultat2 = abs(round(resultat2 * 10000.0f) / 10000.0F);
    resultat3 = abs(round(resultat3 * 10000.0f) / 10000.0f);
    return resultat1;

}

void gradientSimple(float functionF, float deriveePartielleF_x, float deriveePartielleF_y, float pas_simple) {
    std::cout << "Methode de Gradient simple" << std::endl;
    float anciennefonctionF;
    float ancienX[2];
    int cpt = 0;
    do {
        cpt++;
        anciennefonctionF = functionF;
        ancienX[0] = x0[0];
        ancienX[1] = x0[1];
        x0[0] = x0[0] + pas_simple * (-deriveePartielleF_x);
        x0[1] = x0[1] + pas_simple * (-deriveePartielleF_y);
        functionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
    } while (anciennefonctionF > functionF);
    std::cout << "Le minimum de la fonction f(x,y)=(x-y)^2+x^4+y^4 est atteint en : X=(" << ancienX[0] << "," << ancienX[1] << ")" << std::endl;	//X(k+1)=X(k)+pas*d(k) avec d(k) = - laplacien(f(X(k))
}

void gradientOptimal(float fonctionF, float deriveePartielleF_x, float deriveePartielleF_y) {
    std::cout << "Methode de Gradient a pas optimal" << std::endl;
    float optimalStep;
    float anciennefonctionF;
    float ancienX[2];
    int cpt = 0;
    do {
        optimalStep = phiPrime(deriveePartielleF_x, deriveePartielleF_y);
        anciennefonctionF = fonctionF;
        ancienX[0] = x0[0];
        ancienX[1] = x0[1];
        x0[0] = x0[0] + optimalStep * (-deriveePartielleF_x);
        x0[1] = x0[1] + optimalStep * (-deriveePartielleF_y);
        deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0];
        deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1];
        fonctionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
        cpt++;
    } while (anciennefonctionF > fonctionF);
    std::cout << "Le minimum de la fonction f(x,y)=(x-y)^2+x^4+y^4 est atteint en : X=(" << ancienX[0] << "," << ancienX[1] << ")" << "avec pour pas optimal :" << optimalStep << std::endl;
}

void gradientConjugue(float fonctionF, float deriveePartielleF_x, float deriveePartielleF_y) {
    std::cout << "Methode de Gradient conjugue" << std::endl;
    float pas_opti = phiPrime(deriveePartielleF_x, deriveePartielleF_y);
    float anciennefonctionF = fonctionF;
    float ancienX[2];
    float beta_X;
    float beta_Y;
    float normeX;
    float normeY;
    float ancienneNormeX;
    float ancienneNormeY;
    int cpt = 0;

    ancienX[0] = x0[0];
    ancienX[1] = x0[1];
    if (anciennefonctionF <= fonctionF) {
        x0[0] = x0[0] + pas_opti * (-deriveePartielleF_x);
        x0[1] = x0[1] + pas_opti * (-deriveePartielleF_y);
        deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0];
        deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1];
        fonctionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
        normeX = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[0]) * (4 * x0[0]) * (4 * x0[0]);
        normeY = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[1]) * (4 * x0[1]) * (4 * x0[1]);
        cpt++;
    }

    while(anciennefonctionF > fonctionF){
        cpt++;
        pas_opti = phiPrime(deriveePartielleF_x, deriveePartielleF_y);
        anciennefonctionF = fonctionF;
        ancienX[0] = x0[0];
        ancienX[1] = x0[1];
        x0[0] = x0[0] + pas_opti * (-deriveePartielleF_x);
        x0[1] = x0[1] + pas_opti * (-deriveePartielleF_y);
        fonctionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
        ancienneNormeX = normeX;
        ancienneNormeY = normeY;
        normeX = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[0]) * (4 * x0[0]) * (4 * x0[0]);
        normeY = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[1]) * (4 * x0[1]) * (4 * x0[1]);
        beta_X = (normeX) / (ancienneNormeX);
        beta_Y = (normeY) / (ancienneNormeY);
        deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0]+beta_X*deriveePartielleF_x;
        deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1]+beta_Y*deriveePartielleF_y;
    }

    std::cout << "Le minimum de la fonction f(x,y)=(x-y)^2+x^4+y^4 est atteint en : X=(" << ancienX[0] << "," << ancienX[1] << ")" << std::endl;
}

void gradientPolakRibiere(float fonctionF, float deriveePartielleF_x, float deriveePartielleF_y) {
    std::cout << "Methode de Polak-Ribiere" << std::endl;

    float pas_opti = phiPrime(deriveePartielleF_x, deriveePartielleF_y);
    float anciennefonctionF = fonctionF;
    float ancienX[2];
    float beta_X;
    float beta_Y;
    float ancienneNormeX;
    float ancienneNormeY;
    int cpt = 0;
    ancienX[0] = x0[0];
    ancienX[1] = x0[1];
    if (anciennefonctionF <= fonctionF) {
        x0[0] = x0[0] + pas_opti * (-deriveePartielleF_x);
        x0[1] = x0[1] + pas_opti * (-deriveePartielleF_y);
        deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0];
        deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1];
        fonctionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
        cpt++;
    }


    while (anciennefonctionF > fonctionF) {
        cpt++;
        pas_opti = phiPrime(deriveePartielleF_x, deriveePartielleF_y);
        anciennefonctionF = fonctionF;
        ancienX[0] = x0[0];
        ancienX[1] = x0[1];
        x0[0] = x0[0] + pas_opti * (-deriveePartielleF_x);
        x0[1] = x0[1] + pas_opti * (-deriveePartielleF_y);
        fonctionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0] * x0[0] * x0[0] * x0[0] + x0[1] * x0[1] * x0[1] * x0[1];
        ancienneNormeX = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[0]) * (4 * x0[0]) * (4 * x0[0]);
        ancienneNormeY = (2 * x0[0]) * (2 * x0[0]) + (2 * x0[1]) * (2 * x0[1]) + (4 * x0[1]) * (4 * x0[1]) * (4 * x0[1]);
        beta_X = ((2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0]) * ((2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0]) - (2 * ancienX[0] - 2 * ancienX[1] + 4 * ancienX[0] * ancienX[0] * ancienX[0]))) / (ancienneNormeX);
        beta_Y = ((-2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1]) * ((-2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1]) - (-2 * ancienX[0] + 2 * ancienX[1] + 4 * ancienX[1] * ancienX[1] * ancienX[1]))) / (ancienneNormeY);
        deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0] * x0[0] + beta_X * deriveePartielleF_x;
        deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1] * x0[1] * x0[1] + beta_Y * deriveePartielleF_y;
    }

    std::cout << "\"Le minimum de la fonction f(x,y)=(x-y)^2+x^4+y^4 est atteint en : X=(" << ancienX[0] << "," << ancienX[1] << ")" << std::endl;
}

int main(int argc, char *argv[]) {
    int choice;

    std::cout<< "Veuilez selectionner une methode :"<<std::endl;
    std::cout<< "1 : Gradient a pas fixe"<<std::endl;
    std::cout<< "2 : Gradient a pas optimal"<<std::endl;
    std::cout<< "3 : Gradient conjugue"<<std::endl;
    std::cout<< "4 : Polak-Ribiere"<<std::endl;

    std::cin>>choice;
    int initX;
    int initY;
    float x0[2];
    //Function used : f(x,y)=(x-y)^2+x^4+y^4
    float functionF = (x0[0] - x0[1]) * (x0[0] - x0[1]) + x0[0]* x0[0]* x0[0]* x0[0] + x0[1]* x0[1]* x0[1]* x0[1];
    float deriveePartielleF_x = 2 * x0[0] - 2 * x0[1] + 4 * x0[0] * x0[0]* x0[0];
    float deriveePartielleF_y = -2 * x0[0] + 2 * x0[1] + 4 * x0[1]* x0[1]* x0[1];

    switch(choice){
        case 1:
            float fixedStep;
            std::cout<<"GRADIENT A PAS FIXE"<<std::endl;
            std::cout<<"Veuillez rentrer l'abscisse du point initial :"<<std::endl;
            std::cin>>initX;
            std::cout<<"Veuillez rentrer l'ordonnee du point initial :"<<std::endl;
            std::cin>>initY;
            x0[0] = initX;
            x0[1] = initY;

            std::cout<<"Veuillez rentrer la valeur du pas fixe :"<<std::endl;
            std::cin>>fixedStep;

            gradientSimple(functionF, deriveePartielleF_x, deriveePartielleF_y, fixedStep);

            break;
        case 2:

            std::cout<<"GRADIENT A PAS OPTIMAL"<<std::endl;
            std::cout<<"Veuillez rentrer la valeur du point initial (Exemple : 1,1) :"<<std::endl;
            std::cout<<"Veuillez rentrer l'abscisse du point initial :"<<std::endl;
            std::cin>>initX;
            std::cout<<"Veuillez rentrer l'ordonnee du point initial :"<<std::endl;
            std::cin>>initY;

            gradientOptimal(functionF, deriveePartielleF_x, deriveePartielleF_y);
            break;
        case 3:
            std::cout<<"GRADIENT CONJUGUE"<<std::endl;
            std::cout<<"Veuillez rentrer la valeur du point initial (Exemple : 1,1) :"<<std::endl;
            std::cout<<"Veuillez rentrer l'abscisse du point initial :"<<std::endl;
            std::cin>>initX;
            std::cout<<"Veuillez rentrer l'ordonnee du point initial :"<<std::endl;
            std::cin>>initY;
            gradientConjugue(functionF, deriveePartielleF_x, deriveePartielleF_y);
            break;
        case 4:
            std::cout<<"METHODE DE POLAK-RIBIERE"<<std::endl;
            std::cout<<"Veuillez rentrer la valeur du point initial (Exemple : 1,1) :"<<std::endl;
            std::cout<<"Veuillez rentrer l'abscisse du point initial :"<<std::endl;
            std::cin>>initX;
            std::cout<<"Veuillez rentrer l'ordonnee du point initial :"<<std::endl;
            std::cin>>initY;
            gradientPolakRibiere(functionF, deriveePartielleF_x, deriveePartielleF_y);
            break;
        default:
            std::cout<<"Comportement inattendu, fin de programme"<<std::endl;
            exit(-1);
    }
}


